I create new package named models to keep my new classes(Die,DieCup,Game,Piece,Player)

**Die**
-have one attribute that is int named face
-have method roll(), to random the face of itself



**DieCup**

-have List that keep list of Die, in case that we need to use a lot of die.
-The Ctor parameter require the number of die its have.
-have method roll() to roll the the die;



**Game**

-have 3 attributes
* dieCup ------> the cup of dice in this game
* playerList --> keep player list
* boardSize ---> size of our board
* turn --------> to identify player's turn
* checkWin ----> to check the status of game
-Ctor require int boardSite and int numDie
-method getPlayer() to get player1 or player2
-method rollDice() call roll() in dieCup to get the total score we got
-method adjustPosition() require current position and score we got to calculatethe position of each player.
-method resetGame() to reset the game the be default value.
-method checkWin() to check if the player win the game.



**Piece**
-have one int attribute called position, keep current position of player
-have method set, get. to set the position value.


**
Player**
-have 2 attribute
* int ID --->ID of player
* Piece piece ----> piece of each player
-have method getPosition() to call getPosition in piece and return int value.
-have method setPostition() to call setPosition in piece and set the position value.
-have method getID(), return ID of the player


![12874105_790593461071217_1322377192_o.jpg](https://bitbucket.org/repo/84oq5b/images/1412930874-12874105_790593461071217_1322377192_o.jpg)