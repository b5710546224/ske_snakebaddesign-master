package com.ske.snakebaddesign.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ske.snakebaddesign.R;
import com.ske.snakebaddesign.guis.BoardView;
import com.ske.snakebaddesign.models.Game;

import java.util.Observable;
import java.util.Observer;

public class GameActivity extends AppCompatActivity implements Observer{

    private Game game;

    private int p1Position;
    private int p2Position;
    private int turn;

    private int boardSize;
    private BoardView boardView;
    private Button buttonTakeTurn;
    private Button buttonRestart;
    private TextView textPlayerTurn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initComponents() {

        boardSize = 6;
        game = new Game(1,boardSize);
        game.addObserver(this);
        boardView = (BoardView) findViewById(R.id.board_view);
        boardView.setBoardSize(boardSize);

        buttonTakeTurn = (Button) findViewById(R.id.button_take_turn);
        buttonTakeTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeTurn();
            }
        });
        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                game.resetGame();
            }
        });
        textPlayerTurn = (TextView) findViewById(R.id.text_player_turn);
    }


    private void takeTurn() {
        int value = game.rollDice();
        String title = "You rolled a die";
        String msg = "You got " + value;

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
        displayDialog(title, msg, listener);
        game.moveCurrentPiece(value);
    }

    private void displayDialog(String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", listener);
        alertDialog.show();
    }

    @Override
    public void update(Observable observable, Object data) {
        int turn = (int)data;
        if(game.checkWin==0){
            boardView.setP1Position(game.getPlayer1().getPosition());
            boardView.setP2Position(game.getPlayer2().getPosition());
            textPlayerTurn.setText("Player" + (turn + 1) + "'s Turn");
        }else {
            String title = "Game Over";
            String msg = "";
            boardView.setP1Position(0);
            boardView.setP2Position(0);
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialog, int which) {
                    game.resetGame();
                    dialog.dismiss();
                }
            };
                msg = "Player "+game.checkWin+" won!";
            displayDialog(title, msg, listener);
         }
    }
}
