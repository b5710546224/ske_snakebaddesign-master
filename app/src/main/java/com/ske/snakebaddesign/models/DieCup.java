package com.ske.snakebaddesign.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mind on 16/03/2016.
 */
public class DieCup {
    private List<Die> listDie;
    public DieCup(int numDie){
        this.listDie =  new ArrayList<Die>();
        for(int i = 0;i<numDie;i++){
            listDie.add(new Die());
        }
    }

    public int roll(){
        int total = 0;
        for(int i = 0;i<listDie.size();i++){
            total += listDie.get(i).roll();
        }
        return total;
    }
}
