package com.ske.snakebaddesign.models;

/**
 * Created by mind on 16/03/2016.
 */
public class Player {
    private int ID;
    private Piece piece;
    public Player(int ID){
        piece = new Piece();
        this.ID = ID;
    }
    public void setPosition(int position){
        piece.setPosition(position);
    }
    public int getPosition(){
        return piece.getPosition();
    }
    public int getID(){
        return this.ID;
    }
}
