package com.ske.snakebaddesign.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by mind on 16/03/2016.
 */
public class Game extends Observable{
    DieCup dieCup;
    private List<Player> playerList = new ArrayList<Player>();
//    private Player player1;
//    private Player player2;
    private int boardSize;
    private int turn=0;
    public int checkWin = 0;

    public Game(int numDie,int boardSize){
        this.dieCup = new DieCup(numDie);
        this.boardSize = boardSize;
        for(int i = 0;i<2;i++){
            playerList.add(new Player(i+1));
        }
//        player1 = new Player(1);
//        player2 = new Player(0);
    }
    public Player getPlayer1(){
        return this.playerList.get(0);
    }
    public Player getPlayer2(){
        return this.playerList.get(1);
    }
    public int rollDice(){
        return dieCup.roll();
    }

    public int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = boardSize * boardSize - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public void moveCurrentPiece(int value) {
        playerList.get(turn).setPosition(adjustPosition(playerList.get(turn).getPosition(), value));
        checkWin();
        if(turn == 0) turn = 1;
        else turn = 0;
        setChanged();
        notifyObservers(turn);
    }

    public void resetGame() {
        for(int i = 0;i<2;i++)playerList.get(i).setPosition(0);
        checkWin = 0;
        boardSize = 6;
        setChanged();
        notifyObservers(turn);
    }

    public void checkWin() {
        for(int i = 0;i<2;i++) {
            if (playerList.get(i).getPosition() == boardSize * boardSize - 1) checkWin = i + 1;
        }

    }


}